<?php
namespace BNM\WebsiteCore\Command;
    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Database Command
 *
 * Export/import Database
 *
 * @package WebsiteCore
 */

    use TYPO3\CMS\Core\Core\Environment;
    use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class DatabaseCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController {
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager = NULL;

    /**
     * Export Database
     *
     * @param string $dumpFile
     * @return void
     */
    public function exportCommand($dumpFile = 'dump.sql') {
        $dump = Environment::getPublicPath() . '/' . $dumpFile;
        $dbUser = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'];
        $dbPassword = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'];
        $dbHost = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'];
        $dbDatabase = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'];
        $exec = 'mysqldump -u ' . $dbUser . ' -p' . $dbPassword . ' -h ' . $dbHost . ' ' . $dbDatabase . '>' . $dump;
        exec($exec);
        return true;
    }

    /**
     * Import Database
     *
     * @param string $dumpFile
     * @return void
     */
    public function importCommand($dumpFile = 'dump.sql') {
        $dump = Environment::getPublicPath() . '/' . $dumpFile;
        if(is_file($dump)) {
            $dbUser = $GLOBALS['TYPO3_CONF_VARS']['DB']['username'];
            $dbPassword = $GLOBALS['TYPO3_CONF_VARS']['DB']['password'];
            $dbHost = $GLOBALS['TYPO3_CONF_VARS']['DB']['host'];
            $dbDatabase = $GLOBALS['TYPO3_CONF_VARS']['DB']['database'];
            $exec = 'mysql -u ' . $dbUser . ' -p' . $dbPassword . ' -h ' . $dbHost . ' ' . $dbDatabase . '<' . $dump;
            exec($exec);
        }
        return true;
    }
}
