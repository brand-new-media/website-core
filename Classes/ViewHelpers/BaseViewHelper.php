<?php
namespace BNM\WebsiteCore\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/***************************************************************
 *  Copyright notice
*
*  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
*
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * View helper which creates a <base href="..."></base> tag. The Base URI
 * is taken from the current request.
 *
 * = Examples =
 *
 * <code title="Example">
 * {namespace t3b=BNM\T3brick\ViewHelpers}
 * <t3b:base />
 * </code>
 * <output>
 * <base href="http://yourdomain.tld/" />
 * (depending on your domain)
 * </output>
 * 
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package T3brick
 * @subpackage ViewHelpers
 */
class BaseViewHelper extends AbstractTagBasedViewHelper {

	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument('forceProtocol', 'string', 'Force HTTP-Protocol', FALSE, 'auto');
		$this->registerArgument('baseUrl', 'string', 'Fixed base-url', FALSE, '');
	}
	
	/**
	 * Render the "Base" tag by outputting $request->getBaseUri()
	 *
	 * Note: renders as <base></base>, because IE6 will else refuse to display
	 * the page...
	 *
	 * @return string "base"-Tag.
	 * @api
	 */
	public function render() {
		if (TYPO3_MODE == 'BE') {
			return;
		}
		// fixed base url
		if($this->arguments['baseUrl']) {
			$GLOBALS['TSFE']->baseUrl = $this->arguments['baseUrl'];
			return;
		}
		
		$baseDomain = $_SERVER['HTTP_HOST'];
		$baseScheme = $_SERVER['REQUEST_SCHEME'];
		
		switch($this->arguments['forceProtocol']) {
			case 'none':
				$baseUrl = '//' . $baseDomain;
				break;
				
			case 'auto':
				$baseUrl = $baseDomain . '://' . $baseDomain;
				break;
				
			default:
				$baseUrl = $this->arguments['forceProtocol'] . '://' . $baseDomain;
				break;
		}
		$GLOBALS['TSFE']->baseUrl = $baseUrl;
	}
}

?>
