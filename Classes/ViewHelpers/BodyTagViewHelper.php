<?php
namespace BNM\WebsiteCore\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * View helper which creates a <body> tag.
 *
 * = Examples =
 *
 * <code title="Example">
 * {namespace wc=BNM\WebsiteCore\ViewHelpers}
 * <t3b:bodyTag bodyTag="ownbody" bodyTagAdd='id="home"' />
 * </code>
 * <output>
 * <ownbody id='home'>
 * </output>
 * 
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package WebsiteCore
 * @subpackage ViewHelpers
 */
class BodyTagViewHelper extends AbstractTagBasedViewHelper {

	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument('bodyTag', 'string', 'Default body tag', FALSE, 'body');
		$this->registerArgument('bodyTagAdd', 'string', 'Additional body tag attributes', FALSE, '');
	}
	
	/**
	 * Render the "Base" tag by outputting $request->getBaseUri()
	 *
	 * Note: renders as <base></base>, because IE6 will else refuse to display
	 * the page...
	 *
	 * @return string "base"-Tag.
	 * @api
	 */
	public function render() {
		if (TYPO3_MODE == 'BE') {
			return;
		}
        $GLOBALS['TSFE']->pSetup['bodyTag'] = '<' . $this->arguments['bodyTag'];
        $GLOBALS['TSFE']->pSetup['bodyTagAdd'] .= $this->arguments['bodyTagAdd'];
        $GLOBALS['TSFE']->pSetup['bodyTag'] .='>';
    }
}
