<?php
namespace BNM\WebsiteCore\ViewHelpers\Data;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Claus Due <claus@wildside.dk>, Wildside A/S
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Can be used inline, i.e.:
 * <code title="Example">
 * {namespace t3b=BNM\T3brick\ViewHelpers}
 * {t3b:data.record(table: 'tt_content', select: 'uid=123')}
 * </code>
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package T3brick
 * @subpackage ViewHelpers\Data
 */
class ConfigViewHelper extends AbstractViewHelper {

    /**
     * Initialize arguments
     *
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('field', 'string', 'Special field from config-TS', FALSE, FALSE);

    }

    /**
     * "Render" method - sorts a target list-type target. Either $array or
     * $objectStorage must be specified. If both are, ObjectStorage takes precedence.
     *
     * Returns the same type as $subject. Ignores NULL values which would be
     * OK to use in an f:for (empty loop as result)
     *
     *
     * @throws Exception
     * @return mixed
     */
    public function render() {
        $config = $GLOBALS['TSFE']->config['config'];
        if($this->arguments['field']) {
            return $config[$this->arguments['field']];
        }
        return $config;
    }

}
