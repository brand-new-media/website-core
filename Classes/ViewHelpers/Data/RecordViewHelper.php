<?php
namespace BNM\WebsiteCore\ViewHelpers\Data;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Claus Due <claus@wildside.dk>, Wildside A/S
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Can be used inline, i.e.:
 * <code title="Example">
 * {namespace t3b=BNM\T3brick\ViewHelpers}
 * {t3b:data.record(table: 'tt_content', select: 'uid=123')}
 * </code>
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package T3brick
 * @subpackage ViewHelpers\Data
 */
class RecordViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

    /**
     * Initialize arguments
     *
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('table', 'string', 'Database Table', TRUE);
        $this->registerArgument('select', 'string', 'Database Table', FALSE, '*');
        $this->registerArgument('enableFields', 'boolean', 'enableFields', FALSE, TRUE);
        $this->registerArgument('where', 'string', 'Where clause', FALSE, FALSE);
        $this->registerArgument('limit', 'string', 'Limit', FALSE);
        $this->registerArgument('groupBy', 'string', 'Group by', FALSE, '');
        $this->registerArgument('order', 'string', 'Sorting', FALSE, '');
        $this->registerArgument('firstRow', 'string', 'Only first row', FALSE, FALSE);

    }

    /**
     * "Render" method - sorts a target list-type target. Either $array or
     * $objectStorage must be specified. If both are, ObjectStorage takes precedence.
     *
     * Returns the same type as $subject. Ignores NULL values which would be
     * OK to use in an f:for (empty loop as result)
     *
     *
     * @throws Exception
     * @return mixed
     */
    public function render() {
        $where = $this->arguments['where'] ? $this->arguments['where'] : '1=1';
        // enableFields
        $where .= $this->arguments['enableFields'] ? $GLOBALS['TSFE']->cObj->enableFields($this->arguments['table']) : '';
        // Language
        $where .= ' AND (sys_language_uid IN (-1,0) OR (sys_language_uid = "' .
            $GLOBALS['TSFE']->sys_language_uid . '" AND l18n_parent = "0"))';

        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            $this->arguments['select'],
            $this->arguments['table'],
            $where,
            $this->arguments['groupBy'],
            $this->arguments['order']
        );

        while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            // Bei nur einem ergebnis, dass dann zurück geben.
            if($this->arguments['firstRow']) {
                return $row;
            }
            $returnArr[] = $row;
        }
        return $returnArr;
    }

}
