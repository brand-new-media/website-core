<?php
namespace BNM\WebsiteCore\ViewHelpers\Fal;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * View helper which creates a <body> tag.
 *
 * = Examples =
 *
 * <code title="Example">
 * {namespace wat=BNM\WatTemplate\ViewHelpers}
 * Special File-Property
 * <wat.fileRelation uid="123" fieldName="flux_fal" property="fullFilename"/>
 * Full array with file/storage-properties
 * <wat.fileRelation contentUid="123" falField="flux_fal"/>
 * </code>
 * 
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package WatTemplate
 * @subpackage ViewHelpers
 */
 

class FileCollectionViewHelper extends AbstractTagBasedViewHelper {

	/**
	 * File-Collection -Repository
	 *
	 * @var \TYPO3\CMS\Core\Resource\FileCollectionRepository
	 * @inject
	 */
	protected $collectionRepository;
		
	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument('uid', 'integer', 'File-Collection', TRUE);
	}
	
	/**
	 * return Array from FAL object
	 *
	 * @return array.
	 * @api
	 */
	public function render() {
		$collection = $this->collectionRepository->findByUid((int)$this->arguments['uid']);
		
		if (!$collection) {
			return array();
		}
		$falCollection = array();
			
		$collection->loadContents();
		return $collection->toArray();
	}
}

?>
