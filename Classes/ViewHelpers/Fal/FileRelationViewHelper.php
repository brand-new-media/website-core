<?php
namespace BNM\WebsiteCore\ViewHelpers\Fal;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * View helper which creates a <body> tag.
 *
 * = Examples =
 *
 * <code title="Example">
 * {namespace wat=BNM\WatTemplate\ViewHelpers}
 * Special File-Property
 * <wat.fileRelation uid="123" fieldName="flux_fal" property="fullFilename"/>
 * Full array with file/storage-properties
 * <wat.fileRelation contentUid="123" falField="flux_fal"/>
 * </code>
 * 
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package WatTemplate
 * @subpackage ViewHelpers
 */
class FileRelationViewHelper extends AbstractTagBasedViewHelper {

	/**
	 * storageRepository
	 *
	 * @var \TYPO3\CMS\Core\Resource\StorageRepository
	 * @inject
	 */
	protected $storageRepository;
	
	/**
	 * fileRepository
	 *
	 * @var \TYPO3\CMS\Core\Resource\FileRepository
	 * @inject
	 */
	protected $fileRepository;
	
	/**
	 * Initialize arguments
	 *
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument('uid', 'integer', 'tt_content uid', TRUE);
		$this->registerArgument('fieldName', 'string', 'FAL field name', TRUE);
		$this->registerArgument('table', 'string', 'Relation table', FALSE, 'tt_content');
		$this->registerArgument('property', 'string', 'Explicit property return', FALSE, '');
		
	}
	
	/**
	 * return Array from FAL object
	 *
	 * @return array.
	 * @api
	 */
	public function render() {
		$falFileRaw = array_shift($this->fileRepository->findByRelation($this->arguments['table'], $this->arguments['fieldName'], (int)$this->arguments['uid']));
		if(!is_object($falFileRaw)) {
			return;
		}
		return $falFileRaw->toArray();
		
		
		/** 
		 * geht auch, ist aber unnötig!!!
		 */
		$fal = array();
		
		// File properties
		$fal['file'] = $falFileRaw->getProperties();
		
		// Storage properties
		$fal['storage'] = $this->storageRepository->findByUid( (int)$fal['file']['storage'] );
		// Special vars for easy using
// 		$fal['filename'] = $fal['storage']['configuration']['basePath'] . $fal['file']['identifier'];
		$storageConfiguration = $fal['storage']->getConfiguration();
		
		$fal['url'] = 
			$storageConfiguration['basePath'] . 
			$fal['file']['identifier'];
		
		$fal['uid'] = $fal['file']['uid'];
		
		// Special property
		if($this->arguments['property']) {
			return $fal[$this->arguments['property']];
		}
		
		// Return complete infos
		return $fal;
	}
}

?>
