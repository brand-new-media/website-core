<?php

namespace BNM\WebsiteCore\ViewHelpers\Image;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Axel Brand <kontakt@brandnewmedia.it>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * View helper which creates a <body> tag.
 *
 * = Examples =
 *
 * <code title="Example">
 * {namespace wc=BNM\WebsiteCore\ViewHelpers}
 * {wc:GPVar(var: 'print')}
 * </code>
 * <output>
 * <ownbody id='home'>
 * </output>
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>, brand new media
 * @package WebsiteCore
 * @subpackage ViewHelpers
 */

use TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper;

class BootstrapDimensionsViewHelper extends ActionViewHelper
{

    /**
     * Breakpoint, bzw Maximale Breite
     * @var int[]
     */
    protected $maxDimensions = [
        'xs' => 520,
        'sm' => 520,
        'md' => 700,
        'lg' => 940,
        'xl' => 1120
    ];

    /**
     * Breite anhand Bootstrap Spalten
     *
     * @var int[]
     */
    protected $defaultCols = [
        'xs' => 6,
        'sm' => 6,
        'md' => 4,
        'lg' => 3,
        'xl' => 3
    ];

    /**
     * Seitenverhätnis:
     * - aquare => quadratisch
     * - leer oder default => anhand von width und height
     *
     * @var int[]
     */
    protected $ratio = [
        'xs' => 'default',
        'sm' => 'default',
        'md' => 'default',
        'lg' => 'default',
        'xl' => 'default'
    ];

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('width', 'integer', 'Breite', false, 200);
        $this->registerArgument('height', 'integer', 'Höhe', false, 300);
        $this->registerArgument('cols', 'array', 'Spalten', false, $this->defaultCols);
        $this->registerArgument('ratio', 'array', 'Seitenverhätnis', false, $this->ratio);
    }

    public function render()
    {
  //      debug($this->arguments);
        /** @var int $width */
        $width = $this->arguments['width'];

        /** @var int $height */
        $height = $this->arguments['height'];

        /** @var string $ratio */
        if(!($ratio = $this->arguments['ratio'])) {
            $ratio = $this->ratio;
        }

        /**
         * Prüfen ob alles übergeben wurde, sonst mergen
         */
        if(count($ratio) != count($this->ratio)) {
            $ratio = array_merge($this->ratio, $ratio);
        }

        /** @var array $cols */
        if(!($cols = $this->arguments['cols'])) {
            $cols = $this->defaultCols;
        }

        /**
         * Prüfen ob alles übergeben wurde, sonst mergen
         */
        if(count($cols) != count($this->defaultCols)) {
            $cols = array_merge($this->defaultCols, $cols);
//            debug($cols, 'Merge');
        }
        /** @var array $dimensions */
        $dimensions = [];

        /**
         * @var string $viewport
         * @var int $maxWidth
         */
        foreach ($this->maxDimensions as $viewport => $maxWidth) {
            $dimensions[$viewport]['width'] = intval($cols[$viewport] / 12 * $maxWidth);
            switch($ratio[$viewport]) {
                case 'square':
                    $ratioValue = 1;
                    break;

                default:
                    /** @var float $ratioValue */
                    $ratioValue = $width / $height;
            }
            $dimensions[$viewport]['height'] = intval($dimensions[$viewport]['width'] / $ratioValue);
        }
        return $dimensions;
    }
}

?>