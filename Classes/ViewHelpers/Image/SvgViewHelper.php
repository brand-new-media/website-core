<?php

namespace BNM\WebsiteCore\ViewHelpers\Image;


use BNM\Portal\Domain\Model\User;
use BNM\Portal\ViewHelpers\AbstractViewHelper;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;

class SvgViewHelper extends AbstractViewHelper
{
    /**
     * As this ViewHelper renders HTML, the output must not be escaped.
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize Arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('src', 'string', 'Source', true);
        $this->registerArgument('class', 'string', 'CSS-Klassen', false);
        $this->registerArgument('id', 'string', 'ID', false);
    }

    /**
     * Listeneintrag rendern und cachen
     *
     * @return mixed|string
     * @throws NoSuchCacheException
     */
    public function render()
    {
        if($svg = GeneralUtility::getUrl($this->arguments['src'])) {
            $svg = preg_replace('/<title>(.*?)<\/title>/', '', $svg);
            
            if($this->arguments['class']) {
                $svg = str_replace(
                    '<svg',
                    '<svg class="' . $this->arguments['class'] . '"',
                    $svg
                );
            }
            if($this->arguments['id']) {
                $svg = str_replace(
                    '<svg',
                    '<svg id="' . $this->arguments['id'] . '"',
                    $svg
                );
            }
        }
        return $svg;
    }


}
