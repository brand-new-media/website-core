<?php
namespace BNM\WebsiteCore\ViewHelpers;


use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class SpacelessViewHelper extends \TYPO3Fluid\Fluid\ViewHelpers\SpacelessViewHelper
{
    use \TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

    /**
     * @var boolean
     */
    protected $escapeOutput = false;

    /**
     * @param array $arguments
     * @param \Closure $childClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $childClosure, RenderingContextInterface $renderingContext)
    {
        return preg_replace('/<!-- (.|\s)*?-->/', '', parent::renderStatic($arguments, $childClosure, $renderingContext));
    }

}