<?php

$TCA['pages']['columns']['nav_title']['config'] = array(
    'type' => 'text',
    'rows' => 2,
    'cols' => 40
);
$TCA['pages']['columns']['subtitle']['config'] = array(
    'type' => 'text',
    'rows' => 2,
    'cols' => 40
);
$TCA['pages']['columns']['url']['config'] = array(
    'type' => 'text',
    'rows' => 2,
    'cols' => 40
);