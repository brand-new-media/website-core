<?php

/* * *************************************************************
 * Extension Manager/Repository config file for ext "website_core".
 *
 * Auto generated 01-12-2014 17:11
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 * ************************************************************* */

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Site templates',
    'description' => 'Template files and assets for this site',
    'category' => 'misc',
    'shy' => 0,
    'version' => '10.4.1',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearcacheonload' => 1,
    'lockType' => '',
    'author' => '',
    'author_email' => '',
    'author_company' => '',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.5.0-10.4.99',
            'extbase' => '',
            'fluid' => '',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    '_md5_values_when_last_written' => 'a:0:{}',
    'suggests' => array(
    ),
);
