<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Scheduler für Export
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'BNM\\WebsiteCore\\Command\\DatabaseCommandController';


$GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['wc'] = ['BNM\\WebsiteCore\\ViewHelpers'];