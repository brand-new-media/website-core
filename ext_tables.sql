########################################
#
# header in tt_content
#
########################################

CREATE TABLE tt_content (
	header text,
	subheader text
);


########################################
#
# nav_title, subtitle, url  in pages
#
########################################

CREATE TABLE pages (
	nav_title text,
	subtitle text,
	url text
);
